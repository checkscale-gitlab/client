#!/usr/bin/env python3.10

'''
'''

from skyapp.rest.platform import SkyAPI


app = SkyAPI(
    'TEST-SSS',
    'description',
    'version',
    contact={'name': 'sdsd', 'email': 'sdsdsds@sgrg.rr'}
)

if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, port=8008)
