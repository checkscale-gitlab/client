#!/usr/bin/env bash

cd $HOME/.local/lib/facety/web
hypercorn --reload -w 1 --bind 0.0.0.0:10001 index:server