#!/usr/bin/env bash

jupyter notebook \
    --NotebookApp.allow_origin='https://colab.research.google.com' \
    --NotebookApp.disable_check_xsrf=True \
    --NotebookApp.ip=0.0.0.0 \
    --NotebookApp.port=10001 \
    --NotebookApp.port_retries=0 \
    --NotebookApp.open_browser=False \
    --NotebookApp.token='' \
    --NotebookApp.notebook_dir=/facety
