#!/usr/bin/env bash

cd $HOME/.local/lib/facety/rest
hypercorn -w 2 --bind 0.0.0.0:10001 index:app