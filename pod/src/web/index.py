#!/usr/bin/env python3.10
'''
'''

from skyapp.ui.platform import BootstrappedDash


app = BootstrappedDash(
    'skyapp-client-ui'
)


server = app.async_server


if __name__ == '__main__':
    app.debug_server()
